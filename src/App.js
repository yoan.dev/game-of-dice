import React from 'react';
import ReactDOM from 'react-dom';
import ScreenPlaySolo from './Component/Screen/PlaySolo/PlaySolo';
import {
    dataDice,
    maxOfArr,
    randomNumber,
    randomDice,
    arrDiceInJSON,
} from './function/DataDice';

import './App.css';


class App extends React.Component {

    render() {
        return(
            <ScreenPlaySolo
                dataOfDice={dataDice(4,6)}
            />
        );
    }
}

export default App;
