
const operation = [
    (a, b) => a + b,
    (a, b) => a - b,
    (a, b) => a * b
];
    const arrIncreasing = arr => {

        let isIncreasing = 0, change;
        while(isIncreasing !== 'increasing') {
            isIncreasing = true;
            change = 0;
            for(var i = 0; i < arr.length; i++) {
                if(arr[i] > arr[i+1]) {
                    isIncreasing = false;
                    change = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = change;
                } 
            } if (isIncreasing === true) {
                isIncreasing = 'increasing';
            }
        }
        return arr;
    }

    const removeDuplicate = arr => {
        let arrResult = [];
        arr.forEach(element => {
            if (arrResult.indexOf(element) < 0) {
                arrResult.push(element);
            }
        });
        return arrResult;
    }

    const arrOccurrence = arr => {
        let occurrence = 1, index = 0;
        let arrOccurrence = [];
        arr.forEach((e, indice, arr) => {
            if (arr[indice] === arr[indice + 1]) {
                occurrence++;
            } else {
                arrOccurrence[index] = occurrence;
                index++;
                occurrence = 1;
            }
        });
        return arrOccurrence;
    }
    
    const permutator = inputArr => {
        let result = [];
      
        const permute = (arr, m = []) => {
          if (arr.length === 0) {
            result.push(m)
          } else {
            for (let i = 0; i < arr.length; i++) {
              let curr = arr.slice();
              let next = curr.splice(i, 1);
              permute(curr.slice(), m.concat(next))
           }
         }
        }
       permute(inputArr)
       return result;
    }
    const dataOfDice = arr => {
        let result = [], c = 0;
        if (arr.length === 6) {
             for (let k = 0; k < arr.length; k++) {
                 for (let i = 0; i < operation.length; i++) {
                    for (let j = 0; j < operation.length; j++) {
                        if (operation[i] (operation[j] (arr[k][0], arr[k][1]), arr[k][2]) >= 0) {
                            result[c] = operation[i] (operation[j] (arr[k][0], arr[k][1]), arr[k][2]);
                            c++;
                        }
                    }
                }
            }
            return {
                arrDice: arr[0],
                arrNbFindable: removeDuplicate(arrIncreasing(result)),
                arrOccurrence: arrOccurrence(arrIncreasing(result)),
            }
        }
        if (arr.length === 24) {
            for (let k = 0; k < arr.length; k++) {
                for (let i = 0; i < operation.length; i++) {
                    for (let j = 0; j < operation.length; j++) {
                        for (let l = 0; l < operation.length; l++) {    
                        if (operation[i] (operation[j] (operation[l] (arr[k][0], arr[k][1]), arr[k][2]), arr[k][3]) >= 0) {
                            result[c] = operation[i] (operation[j] (operation[l] (arr[k][0], arr[k][1]), arr[k][2]), arr[k][3]);
                            c++;
                        }
                        }
                    }
                }
            }
            return {
                arrDice: arr[0],
                arrNbFindable: removeDuplicate(arrIncreasing(result)),
                arrOccurrence: arrOccurrence(arrIncreasing(result)),
            }
        }
        if (arr.length === 120) {
            for (let k = 0; k < arr.length; k++) {
                for (let i = 0; i < operation.length; i++) {
                    for (let j = 0; j < operation.length; j++) {
                        for (let l = 0; l < operation.length; l++) {
                            for (let m = 0; m < operation.length; m++) {
                                if (operation[i] (operation[j] (operation[l] (operation[m] (arr[k][0], arr[k][1]), arr[k][2]), arr[k][3]), arr[k][4]) >= 0) {
                                    result[c] = operation[i] (operation[j] (operation[l] (operation[m] (arr[k][0], arr[k][1]), arr[k][2]), arr[k][3]), arr[k][4]);
                                    c++;
                                }
                            }
                        }
                    }
                }
            }
            return {
                arrDice: arr[0],
                arrNbFindable: removeDuplicate(arrIncreasing(result)),
                arrOccurrence: arrOccurrence(arrIncreasing(result)),
            }
        }
    }
    export const arrDiceInJSON = arr => {

        const result = [];
        let diceJSON = {};
      
        arr.forEach((item, index) => {
            diceJSON.value = item;
            diceJSON.isSmall = false;
            diceJSON.id = index;
            result.push(diceJSON);
            diceJSON={};
        });
        return result;
      }
    export const maxOfArr = arr => {
        var getMax = arr[0];
        for (let i = 1; i < arr.length; i++) {
            if (getMax < arr[i]) {
                getMax = arr[i];
            }
        }
        return getMax;
    }
    export const randomNumber = (arr1, arr2) => {
        const randomNumber = Math.floor(Math.random() * arr1.length);
        return {
            nbToFind: arr1[randomNumber],
            nbOfOccurrence: arr2[randomNumber]
        };
    }
    export const randomDice = maxValue => {
        return Math.floor(Math.random() * maxValue) + 1;
    }

export const dataDice = (nbDes, maxValue) => {
    const arrDice = () => {
        let arrDice = [];
        for(let i = 0; i<nbDes; i++) {
            arrDice[i] = randomDice(maxValue);
        }
        return arrDice;
    }
    let throwOfDice = () => {
        return dataOfDice(permutator(arrDice()));
    }
    return throwOfDice();
}

export const dynamicWarning = div => {

    const isWrong = (div) => {
        let count = 0;
        const id = setInterval(function() {
            warning(div);
            count++;
            if (count === 8) {
                div.style.border='none';
                clearInterval(id);
            }
        }, 80);
    }
    
    const warning = div => {
       const divMargLeft = parseInt(getComputedStyle(div).marginLeft);
       const divMargRight = parseInt(getComputedStyle(div).marginRight);
       div.style.border='solid 2px red';
       if (divMargLeft > 0) {
            div.style.marginLeft = (divMargLeft - 4) + 'px';
            div.style.marginRight = (divMargRight + 4) + 'px';
       } if (divMargLeft < 1) {
            div.style.marginLeft = (divMargLeft + 4) + 'px';
            div.style.marginRight = (divMargRight - 4) + 'px';
       }
    }
    isWrong(div);
}



    






