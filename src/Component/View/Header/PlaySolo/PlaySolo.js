import React from 'react';
import ReactDOM from 'react-dom';
import BtnHome from '../../../Btn/Home/Home';
import './PlaySolo.css';

class ViewHeaderPlaySolo extends React.Component {
    render() {
        return(
            <div className="view__header__playSolo">
                <BtnHome/>
            </div>
        );
    }
}

export default ViewHeaderPlaySolo;