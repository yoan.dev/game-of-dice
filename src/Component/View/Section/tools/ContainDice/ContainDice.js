import React from 'react';
import ReactDOM from 'react-dom';
import BtnDice from '../../../../Btn/Dice/Dice';
import './ContainDice.css';

class ViewSectionToolsContainDice extends React.Component {

    
    
    render() {
        const {arrDices} = this.props;
        return(
            <div className="view__section__tools_containDice">
                {arrDices}
            </div>
        );
    }
}

export default ViewSectionToolsContainDice;