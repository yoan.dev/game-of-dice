import React from 'react';
import ReactDOM from 'react-dom';
import {dynamicWarning} from '../../../../../function/DataDice';
import './Result.css';


class ViewSectionToolsResult extends React.Component {


    handleWrong(div) {
        dynamicWarning(div);
        setTimeout(this.props.onWrong, 640);
    }
    render() {
        const {arrMiniDices, result, isWrong} = this.props;
        const displayResult = isWrong === true ? "Wrong !" : result;
        const style = isWrong === true ? {color: 'red'} : {color: 'black'};
        return(
            <div className="view__section__tools__result">
                {isWrong ? this.handleWrong(this.refs.warningDiv): null}
                <div className="view__section__tools__result__dice" ref="warningDiv">
                    {arrMiniDices}
                </div>
                <input 
                className="view__section__tools__result__nb" 
                value={displayResult} 
                style={style}
                readOnly
                />
            </div>
        );
    }
}

export default ViewSectionToolsResult;