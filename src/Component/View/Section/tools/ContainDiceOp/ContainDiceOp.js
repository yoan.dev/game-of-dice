import React from 'react';
import ReactDOM from 'react-dom';
import ViewSectionToolsContainDice from '../ContainDice/ContainDice';
import ViewSectionToolsContainOp from '../ContainOp/ContainOp';
import './ContainDiceOp.css';

class ViewSectionToolsContainDiceOp extends React.Component {
    render() {
        const {arrDices, onOperator} = this.props;
        return(
            <div className="view__section__tools__containDiceOp">
                <ViewSectionToolsContainDice 
                    arrDices={arrDices}
                />
                <ViewSectionToolsContainOp
                    onOperator={onOperator}
                />
            </div>
        );
    }
}

export default ViewSectionToolsContainDiceOp;