import React from 'react';
import ReactDOM from 'react-dom';
import './FoundNb.css';

class ViewSectionToolsFoundNb extends React.Component {
    render() {
        const {foundNumber, timer} = this.props;
        return(
            <div className="view__section__tools__foundNb">
                <p className="view__section__tools__foundNb__text">
                trouver le nombre {foundNumber}
                </p>
                <div className="view__section__tools__foundNb__timer">{timer}s</div>
            </div>
            
        );
    }
}

export default ViewSectionToolsFoundNb;