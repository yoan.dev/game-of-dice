import React from 'react';
import ReactDOM from 'react-dom';
import BtnOperator from './../../../../Btn/Operator/Operator';
import './ContainOp.css';

class ViewSectionToolsContainOp extends React.Component {

    render() {
        const {onOperator} = this.props;
        return(
            <div className="btns__op">
                <BtnOperator
                    onClick={onOperator}
                    className="btn__op"
                    classNameImg="btn__op__img"
                    value="+"
                />
                <BtnOperator
                    onClick={onOperator}
                    className="btn__op"
                    classNameImg="btn__op__img"
                    value="-"
                />
                <BtnOperator
                    onClick={onOperator}
                    className="btn__op"
                    classNameImg="btn__op__img"
                    value="x"
                />
            </div>
        );
    }
}

export default ViewSectionToolsContainOp;