import React from 'react';
import ReactDOM from 'react-dom';
import ViewSectionToolsFoundNb from '../tools/FoundNb/FoundNb';
import ViewSectionToolsResult from '../tools/Result/Result';
import ViewSectionToolsContainDiceOp from '../tools/ContainDiceOp/ContainDiceOp';
import BtnDice from '../../../Btn/Dice/Dice';
import BtnOperator from '../../../Btn/Operator/Operator';
import {timer} from '../../../../function/DataDice';
import './PlaySolo.css';

class ViewSectionPlaySolo extends React.Component {
    constructor(props) {
        super(props);
        this.handleOperator = this.handleOperator.bind(this);
        this.handleWrong = this.handleWrong.bind(this);
        this.state = {
            listDices: this.props.listDices,
            countOp: 0,
            result: '',
            listOrderResult: [],
            listResult: [],
            time: 20,
            idTime: null,
        }
    }

    handleOrderOfDices(id) {

        const {listOrderResult, listResult} = this.state;
        const [listResultCopy, listOrderResultCopy] = [listResult.slice(), listOrderResult.slice()];
        let listDices = [...this.state.listDices];
        let diceOf = {...listDices[id]};
        const dice = this.state.listDices.find(dice => dice.id === id);
        if (dice.isSmall) {
            const getIndex = listOrderResultCopy.indexOf(dice.id);
            listOrderResultCopy.splice(getIndex, 2);
            listResultCopy.splice(getIndex, 2);
            diceOf.isSmall = !diceOf.isSmall;
        } else if (!listResultCopy.length || typeof listResultCopy[listResultCopy.length-1]==="string") {
            listOrderResultCopy.push(dice.id);
            listResultCopy.push(dice.value)
            diceOf.isSmall = !diceOf.isSmall;
        }
        listDices[id] = diceOf;

        this.setState({
            listDices,
            listOrderResult: listOrderResultCopy,
            listResult: listResultCopy,
        });
        this.handleCalculator(listResultCopy);
    }
    handleOperator(e, op, key) {
        const {listOrderResult, listResult, countOp} = this.state;
        let countOpCopy = countOp;
        const [listOrderResultCopy, listResultCopy] = [listOrderResult.slice(), listResult.slice()];

        if (e.target.className === "btn__op__img" && listOrderResultCopy.length !==0 ) {
            if(listOrderResultCopy[listOrderResultCopy.length-1].toString().charAt(0) === "o") {
                listOrderResultCopy.pop();
                listResultCopy.pop();
                countOpCopy--;
            }
            listOrderResultCopy.push('op'+countOpCopy);
        listResultCopy.push(op);
        countOpCopy++;
        } else {
            const getIndex = listOrderResultCopy.indexOf(key);
            console.log(getIndex)
        }
        
        this.setState({
            listOrderResult: listOrderResultCopy,
            listResult: listResultCopy,
            countOp: countOpCopy,
        });
        
    }
    handleCalculator(listResult) {
        let result = this.state.result, operator, operande;
        result = listResult[0] === undefined ? '' : listResult[0];
        for (let i = 1, c = listResult.length; i < c; i+=2) {
            if (listResult[i+1] !== undefined) {
                operator = listResult[i];
                operande = listResult[i+1];
                if (operator === '+') result = result + operande;
                if (operator === '-') result = result - operande;
                if (operator === 'x') result = result * operande;
            }
        }
        this.setState({result});
        return this.state.result;
    }
    handleWrong() {
        this.setState({
            listDices: this.props.listDices,
            listOrderResult: [],
            listResult: [],
            result: '',
            countOp: 0,
        });
    }
    loseByTimer() {
        this.props.onLose();
    }
    componentDidMount() {
        let counter = 19;
        let intervalId = null;
        const finish = () => {
            clearInterval(intervalId);
            this.loseByTimer();
            //dire perdu
        }
        const bip = () => {
            this.setState({time: counter});
            counter--;
            
            if (counter === -1){ 
                this.setState({time: counter});
                finish();
            }
        }
        const start = () => {
            intervalId = setInterval(bip, 1000);
            this.setState({idTime: intervalId});
        }
        start();
    
    }
    render() {
        
        const {listDices, listOrderResult, listResult, result, time} = this.state;
        const {foundNumber, onWin} = this.props;
        const arrMiniDices = [];
        let countOp=0;
        let countAll=0;
        console.log(time);

        listOrderResult.map(id => {
            if (typeof id === "number") {
                countAll++;
                const item = listDices.find(dice => dice.id === id);
                if (item.isSmall) {
                    arrMiniDices.push(
                        <BtnDice 
                            key={item.id}
                            value={item.value}
                            className="btn__dice--small"
                            onClick={() => this.handleOrderOfDices(item.id)}
                        />
                    )
                }   
            } else {
                arrMiniDices.push(
                    <BtnOperator
                        key={"op" + countOp}
                        dataKey={"op" + countOp}
                        className="btn__op--small"
                        classNameImg="btn__op__img--small"
                        value={listResult[countAll]}
                        onClick={this.handleOperator}
                    />
                );
                countOp++;
                countAll++;
            }
            
        });
        const arrDices = 
        listDices.map(item => {
            if (!item.isSmall) {
               return(
                   <BtnDice 
                        key={item.id}
                        value={item.value}
                        className="btn__dice"
                        onClick={() => this.handleOrderOfDices(item.id)}
                    />
               );
            }
        });
        if(result === foundNumber && listResultLength['dices' + listDices.length] === listResult.length) {
            clearInterval(this.state.idTime);
            onWin();
        }
        const isWrong = result !== foundNumber && listResultLength['dices' + listDices.length] === listResult.length ? true : false;
        
        return(
            <div className="view__section__playSolo">
                <ViewSectionToolsFoundNb
                    foundNumber={foundNumber}
                    timer={this.state.time}
                />
                <ViewSectionToolsResult
                    arrMiniDices={arrMiniDices}
                    result={result}
                    isWrong={isWrong}
                    onWrong={this.handleWrong}

                />
                <ViewSectionToolsContainDiceOp
                    arrDices={arrDices}
                    onOperator={this.handleOperator}
                />
            </div>
        );
    }
}

const listResultLength = {
    dices3: 5,
    dices4: 7,
    dices5: 9,
};

export default ViewSectionPlaySolo;
