import React from 'react';
import ReactDOM from 'react-dom';

import './Home.css';

class BtnHome extends React.Component {
    render() {
        return(
            <div>
                <img
                className="btn__home"
                src={require('../../../img/home.png')}
                alt="home"
                />
            </div>
        );
    }
}

export default BtnHome;