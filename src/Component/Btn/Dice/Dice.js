import React from 'react';
import ReactDOM from 'react-dom';
import './Dice.css';


class BtnDice extends React.Component {

    renderDice() {
        const {value, className} = this.props;
        const image = require('../../../img/des' + value + '.png');
        const ImgclassName = className === "btn__dice" ?
        "btn__dice__img" : "btn__dice__img--small";
        
        return <img 
                className={ImgclassName}
                src={image} 
                alt="des"
                /> 
    }
    
    render() {
        const {className='', onClick} = this.props;

        return(
            <div
            className={className}
            onClick={onClick}
            draggable="false"
            dragstart="false"
            >
                {this.renderDice()}
            </div>
        );
    }
}

export default BtnDice;