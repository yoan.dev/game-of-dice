import React from 'react';
import ReactDOM from 'react-dom';
import './Operator.css';

const opNames = {
    "+": "plus",
    "-": "moins",
    "x": "multiple",
};

class BtnOperator extends React.Component {

    render() {
        const {className='', onClick, value, classNameImg, dataKey} = this.props;
        return(
                <div
                 className={className}
                 onClick={(e) => this.props.onClick(e, value, dataKey)}
                 >
                    <img className={classNameImg} src={require('../../../img/' + opNames[value]+ '.png')} alt="moins"/>
                </div>
        );
    }
}

export default BtnOperator;