import React from 'react';
import ReactDOM from 'react-dom';
import ViewHeaderPlaySolo from '../../View/Header/PlaySolo/PlaySolo';
import ViewSectionPlaySolo from '../../View/Section/PlaySolo/PlaySolo';
import {arrDiceInJSON} from '../../../function/DataDice';
import {randomNumber, } from '../../../function/DataDice';
import './PlaySolo.css';


class ScreenPlaySolo extends React.Component {
    constructor(props) {
        super(props);
        this.handleWin = this.handleWin.bind(this);
        this.state = {
            isWin: false,
            loseByTimer: false,
        };
    }
    handleWin() {
        console.log('win')
        this.setState({isWin: true});
    }
    handleLose() {
        console.log('Lose');
        this.setState({loseByTimer: true});
    }
    foundNumber() {
        const {dataOfDice} = this.props;
        let foundNumber = randomNumber(dataOfDice.arrNbFindable, dataOfDice.arrOccurrence).nbToFind;
         while (foundNumber > 100) {
            foundNumber = randomNumber(dataOfDice.arrNbFindable, dataOfDice.arrOccurrence).nbToFind;
        }
        
        return foundNumber;
    }

    render() {

        const {isWin, loseByTimer} = this.state;
        const {dataOfDice} = this.props;
        return(
            <div className="screen__playSolo">
                <ViewHeaderPlaySolo />
                {!loseByTimer ? (!isWin
                ? <ViewSectionPlaySolo 
                    listDices={arrDiceInJSON(dataOfDice.arrDice)}
                    foundNumber={this.foundNumber()}
                    onWin={() => this.handleWin()}
                    onLose={() => this.handleLose()}
                />
                : <p>you Win ! </p>)
                : <p>you Lose...Timer out !</p>
                }
            </div>
            
        );
    }
}

export default ScreenPlaySolo;
